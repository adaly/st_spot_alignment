import cv2
import numpy as np
from matplotlib import pyplot as plt
from plotting import plotCompare, drawKeypoints, curateKeypoints

#################################################################################
##########				IMAGE PROCESSING ROUTINES				#################
#################################################################################

import tissue_recognition as tr
from PIL import Image, ImageOps
Image.MAX_IMAGE_PIXELS = 10e10

# Apply a sequence of morphology transforms to the image.
def applyMT(image, operations):
	kernel = np.array([[0,1,0],[1,1,1],[0,1,0]])
	result = image

	for name, args in operations:	
		if "kernel" in args.keys():
			kernel = args["kernel"]
		if name == "OPENING":
			result = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
	return result

# Processes PIL images for spot detection. Performs:
# - Optional inversion (for Cy3 images), to ensure that features are black on white background.
# - Adaptive histogram equalization, using OpenCV CLAHE routine.
# - Adaptive mean thresholding, using OpenCV.
# and returns a PIL image.
def applyBCT(image, apply_thresholding=True, apply_inversion=True, block_size=None):
	# The image is (optionally) inverted to colour the features darkest
	if apply_inversion:
		image = ImageOps.invert(image)

	# Convert the image into a grayscale
	image = np.array(image.convert('L'))

	# Create a CLAHE object
	clahe = cv2.createCLAHE(clipLimit=20.0, tileGridSize=(1, 1))
	image = clahe.apply(image)

	if apply_thresholding:
		# Block size defines region for mean adaptive thresholding (must be odd-valued)
		# If set too small (below the size of a spot) then the spots will be "hollowed" due to centers 
		#   of spots failing to exceed mean threshold.
		# Block size of 103 was optimal for images with long dimension ~6800, so scale with image size.	
		if block_size is None:
			block_size = int(103./6800 * max(image.shape))
		elif not isinstance(block_size, int):
			raise ValueError("Block size for adaptive thresholding must be an odd-valued integer")
		if block_size % 2 == 0:
			block_size += 1
		# Mean adaptive threshold has been chosen here because Gaussian
		# adaptive thresholding is very slow, takes about 15 minutes for a
		# 20k x 20k image and does not yield significantly better results
		image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 
			block_size, 20)

	return Image.fromarray(image).convert('RGB')

# Given aligned HE and Cy3 image arrays (and the Spot object describing the Cy3 grid):
# - Straightens both images so that rows of spot grid are perfectly horizontal.
# - Crops both images to the area of the Cy3 spot grid (plus some margin).
# - Creates a binarized, masked version of the Cy3 file only showing spots under tissue.
def straightenCropAndMask(cy3img,heimg,cy3spots,margin=0):
	# Rotate images so that the spots are horizontally/vertically aligned.
	center, angle = cy3spots.get_rotation()
	Mrot = cv2.getRotationMatrix2D(center,angle,1.0)
	
	cy3img_rot = cv2.warpAffine(cy3img, Mrot, (cy3img.shape[1],cy3img.shape[0]))
	cy3img_bin = np.array(applyBCT(Image.fromarray(cy3img_rot), apply_inversion=True).convert("L"))
	heimg_rot = cv2.warpAffine(heimg, Mrot, (heimg.shape[1],heimg.shape[0]))

	# Calculate bounding box of Cy3 spot grid
	xmin = int(max(cy3spots.tl[0]-margin,0))
	xmax = int(min(cy3spots.br[0]+margin,cy3img.shape[0]))
	ymin = int(max(cy3spots.tl[1]-margin,0))
	ymax = int(min(cy3spots.br[1]+margin,cy3img.shape[1]))

	print("Computing tissue mask...")
	# Tissue recognition takes an absurdly long time for images sized like our inputs (approx 6000x6000).
	# Instead, we will calculate a bounding box on a compressed version then scale it back up.
	shrink_factor = float(max(heimg.shape))/1000
	new_w, new_h = int(heimg.shape[0]//shrink_factor), int(heimg.shape[1]//shrink_factor)
	mask = np.zeros((new_w,new_h),dtype=np.uint8)
	hesmall = cv2.resize(heimg_rot,(new_h,new_w))
	tr.recognize_tissue(hesmall,mask)
	bin_mask = tr.get_binary_mask(mask)
	bin_mask = cv2.resize(bin_mask,(cy3img.shape[1],cy3img.shape[0]))

	# White out area outside tissue on binarized Cy3 image
	inv_mask = bin_mask < bin_mask.mean()
	cy3img_bin[inv_mask]=255

	heimg_crop = heimg_rot[ymin:ymax,xmin:xmax]
	cy3img_crop = cy3img_rot[ymin:ymax,xmin:xmax]
	cy3img_bin_crop = cy3img_bin[ymin:ymax,xmin:xmax]

	return cy3img_crop, heimg_crop, cy3img_bin_crop


#################################################################################
##########				IMAGE ALIGNMENT ROUTINES				#################
#################################################################################

import sys
import random
from spots import Spots
from skimage import transform as tf
from imageio import imread

''' Simple blob detection for finding spots in Cy3/HE images.
'''
def detectBlobs(img, isHE=True, discard=0.0):
	params = cv2.SimpleBlobDetector_Params()
	
	params.filterByArea = True

	# Some of these parameters have been calibrated for images with approximate dimension 6800x6200
	maxDim = max(img.shape)
	scale = maxDim/6800

	if isHE:
		# These parameters might become more important when looking at HE images.
		params.minThreshold = 20
		params.maxThreshold = 255

		# Spots have an approx. radius of 40, so area ~= 5000.
		params.minArea = 2500 * scale**2
		params.maxArea = 15000 * scale**2

		# Observed from an example image -- average center-center distance ~= 150px.
		params.minDistBetweenBlobs = 100 * scale

		# Default convexity settings are quite high, and remove a lot of noisy spots.
		params.filterByConvexity = True
		params.minConvexity = 0.6

		# Prevents elliptical or other convex, oblong artifacts from being detected.
		params.filterByInertia = True
		params.minInertiaRatio = 0.65
	else:
		params.minArea = 1500 * scale**2
		params.maxArea = 10000 * scale**2
		params.minDistBetweenBlobs = 100 * scale

	detector = cv2.SimpleBlobDetector_create(params)
	keypoints = detector.detect(img)

	# Optionally discard a defined fraction of extreme points (largest and smallest) by size to eliminate outliers.
	if discard > 0:
		discard = min(1.0, discard)
		keypoints.sort(key=lambda x: x.size)
		margin = int(discard/2 * len(keypoints))
		return keypoints[margin:len(keypoints)-margin]

	return keypoints

"""	Helper function for slideGrid()
	Finds all corresponding spots when HE spot grid is aligned to Cy3 spot grid at offset (xoff,yoff) from upper left.
	Returns coordinates and sizes of all such matches.
"""
def _find_all_matches(cy3_spots,he_spots,xoff,yoff):
	cy3pts, hepts = [],[]
	cy3rad, herad = [],[]
	for i in range(he_spots.array_size[0]):
		for j in range(he_spots.array_size[1]):
			if he_spots.is_spot_at_gridpoint(i,j) and cy3_spots.is_spot_at_gridpoint(i+xoff,j+yoff):
				h = he_spots.get_spot_at_gridpoint(i,j)
				hr = he_spots.get_spot_size_at_gridpoint(i,j)
				c = cy3_spots.get_spot_at_gridpoint(i+xoff,j+yoff)
				cr = cy3_spots.get_spot_size_at_gridpoint(i+xoff,j+yoff)
				hepts.append(h)
				herad.append(hr)
				cy3pts.append(c)
				cy3rad.append(cr)
	return cy3pts,hepts,cy3rad,herad

""" Helper function for slideGrid()
	Calculates total overlap between corresponding spots found by _find_all_matches().
	For each overlapping spot, sums the diameter of the overlapping region.
"""
def _calculate_overlap(pts1,pts2,rs1,rs2):

	def _overlap(c1,c2,r1,r2):
		d = np.sqrt(np.sum((c1-c2)**2))
		# If circles are very close, overlap is the diameter of the smaller circle.
		if d <= 1e-10:
			return 2 * min(r1,r2)
		# If distance is wider than sum of radii, overlap is 0.
		elif d >= r1+r2:
			return 0
		else:
			return d - (d-r1) - (d-r2)

	overlap = 0
	for c1,c2,r1,r2 in zip(pts1,pts2,rs1,rs2):
		o = _overlap(c1,c2,r1,r2)
		assert o>=0, "Overlap should always be positive!"
		overlap += o			
	return overlap

""" Accepts numpy arrays representing both Cy3 and HE images, and Spot objects calculated from said images.
	For each potential grid alignment between Cy3 and HE, a partial affine transformation (transpose, rotate, 
	scale) will be calculated using all matched grid points.
"""
def slideGrid(cy3_spots, he_spots):
	slide_x = cy3_spots.array_size[0]-he_spots.array_size[0]+1
	slide_y = cy3_spots.array_size[1]-he_spots.array_size[1]+1

	if slide_x < 0 or slide_y < 0:
		raise ValueError('Detected grid in HE image exceeds that of Cy3 image in at least one dimension!')

	best_M = None
	maxOverlap = 0
	N_ANCHORS = 3

	def _check_alignment(x,y):
		C,H,Cr,Hr = _find_all_matches(cy3_spots,he_spots,x,y)
		
		# Not enough matching points in current alignment to define a similarity transform.
		if len(H) < N_ANCHORS:
			return 0, None

		src = np.float32(H)
		dst = np.float32(C)

		# AffinePartial2D considers only translation, rotation, and linear scaling (no skew)
		# - also known as a "Similarity Transform"
		M, inliers = cv2.estimateAffinePartial2D(src,dst,cv2.RANSAC)
						
		if M is not None:
			# Transform the anchor points in the HE image and find overlap.
			H_warp = [np.matmul(M,np.transpose(p+[1])) for p in H]
			o = _calculate_overlap(C,H_warp,Cr,Hr)
			return o, M
		else:
			return 0, None

	# Special case for when full sized grid is detected in HE image (no sliding required)
	if slide_x == 0 and slide_y == 0:
		maxOverlap, best_M = _check_alignment(0,0)
	else:
		for x in range(slide_x):
			for y in range(slide_y):
				o, M = _check_alignment(x,y)
				if o > maxOverlap:
					maxOverlap = o
					best_M = M

	if maxOverlap == 0:
		raise ValueError("No overlap found for any alignment of current grids.")

	return maxOverlap, best_M

""" Accepts a Spot object, iterates through all keypoints associated with that object, and recomputes the minimum bounding grid
	with each keypoint removed. 
	Returns an array of remaining keypoints and associated Spot object.
"""
def removeWorstKeypoint(keypoints, minSpotDist, arraySize):
	maxDist = np.inf
	bestGrid,bestKeypoints = None, None

	n = len(keypoints)
	for i in range(n):
		allExcept = keypoints[0:i] + keypoints[i+1:n]
		newGrid = Spots(arraySize,1.0)
		newGrid.create_spots_from_keypoints_sparse(allExcept,minSpotDist)
		dist = newGrid.distance_from_grid()

		if dist < maxDist:
			maxDist = dist
			bestGrid = newGrid
			bestKeypoints = allExcept
	return bestKeypoints, bestGrid

''' Accepts file paths pointing to Cy3 and HE images, along with the dimensions of the ST grid:
		[SPOTS_ACROSS, SPOTS_DOWN]
	and optional "display" argument, which triggers the display of detected spots and their predicted grid positions in both
	images, and is useful for debugging.
'''
def alignImages(cy3file, hefile, arraySize, display=False, postProcessing=False, saveTo=None, presentGUI=False, mtTransforms=None):
	# Open images using PIL library, apply image adjustment and thresholding, then export as numpy arrays for blob detection.
	cy3img = Image.open(cy3file)
	cy3img_bct = applyBCT(cy3img)
	cy3img_arr = np.array(cy3img_bct.convert('L'))

	heimg = Image.open(hefile)
	heimg_bct = applyBCT(heimg,apply_inversion=False)
	heimg_arr = np.array(heimg_bct.convert('L'))

	# TODO: Should be able to specify which preprocessing operations are applied to which image (HE or Cy3).
	if mtTransforms is not None:
		cy3img_arr = applyMT(cy3img_arr, mtTransforms)
		heimg_arr = applyMT(heimg_arr, mtTransforms)

	# Run blob detection on both images and extract keypoints.
	cy3_keypoints = detectBlobs(cy3img_arr,isHE=False)
	print(len(cy3_keypoints), "spots detected in Cy3 file.")
	
	he_keypoints = detectBlobs(heimg_arr,isHE=True)
	print(len(he_keypoints), "spots detected in HE file.")

	if presentGUI:
		cy3_keypoints = curateKeypoints(cy3img_arr, cy3_keypoints)
		spot_sizes = [k.size for k in cy3_keypoints]
		avg_spot_size, min_spot_size, max_spot_size = np.mean(spot_sizes), np.min(spot_sizes), np.max(spot_sizes)
		he_keypoints = curateKeypoints(heimg_arr, he_keypoints, avg_spot_size)

	MIN_SPOTS_HE = 4
	if len(he_keypoints) < MIN_SPOTS_HE:
		raise RuntimeError("Unable to detect sufficient number of spots in HE file for alignment.")

	# Snap detected spots in Cy3 image to a grid.
	cy3_spots = Spots(arraySize,1.)
	cy3_spots.create_spots_from_keypoints(cy3_keypoints, cy3img_bct)

	# Find minimal grid containing spots in HE image, using between-spot distances in Cy3 image as an estimate for row/column width.
	xspace = (cy3_spots.br[0]-cy3_spots.tl[0])/cy3_spots.array_size[0]
	yspace = (cy3_spots.br[1]-cy3_spots.tl[1])/cy3_spots.array_size[1]
	he_spots = Spots(arraySize,1.0)
	he_spots.create_spots_from_keypoints_sparse(he_keypoints,(xspace,yspace))

	# Remove the N_TRIM worst keypoints, as measured by biggest improvement in grid fitting when they're gone.
	if not presentGUI:
		N_TRIM = min(3, len(he_keypoints)-MIN_SPOTS_HE)
		for i in range(N_TRIM):
			he_keypoints, he_spots = removeWorstKeypoint(he_keypoints,(xspace,yspace),arraySize)

	# Show spots in Cy3 and HE image used for alignment, as well as the detected grid
	if display:
		cy3_xmeans, cy3_ymeans = cy3_spots.xmeans, cy3_spots.ymeans
		drawKeypoints(cy3img_arr,cy3_keypoints,xgrid=cy3_xmeans,ygrid=cy3_ymeans)
		
		he_xmeans, he_ymeans = he_spots.xmeans, he_spots.ymeans	
		drawKeypoints(heimg_arr,he_keypoints,xgrid=he_xmeans,ygrid=he_ymeans)

	# "Slide" smaller HE spot grid across Cy3 grid, and find alignment that produces minimal distance between matching spots.
	M = None
	while M is None and len(he_keypoints) >= MIN_SPOTS_HE:
		try:
			d, M = slideGrid(cy3_spots, he_spots)
			heimg_align = cv2.warpAffine(heimg_arr, M, (cy3img_arr.shape[1],cy3img_arr.shape[0]))
		except ValueError as err:
			print(err)
			print('Removing "worst" keypoint and recomputing grid...')
			he_keypoints, he_spots = removeWorstKeypoint(he_keypoints, (xspace,yspace),arraySize)
			print(len(he_keypoints),"remain; grid size =",he_spots.array_size)
	if M is None:
		raise RuntimeError("Could not find valid alignment.")

	print("Similarity Transform applied to HE image:",M)

	# Display the final alignment with keypoints
	for kp in he_keypoints:
		pt3d = np.array(kp.pt+(1,))
		kp.pt = tuple(np.matmul(M,np.transpose(pt3d)))
	alpha = 0.75
	blended_img = cv2.addWeighted(heimg_align,alpha,cy3img_arr,1-alpha,0)
	if saveTo is None:
		drawKeypoints(blended_img,he_keypoints)
	else:
		drawKeypoints(blended_img,he_keypoints, saveTo=saveTo+"_overlay.jpg")

	# Straighten and crop images
	if postProcessing:
		heimg_align_color = cv2.warpAffine(np.array(heimg), M, (cy3img_arr.shape[1],cy3img_arr.shape[0]))
		cy3img_output, heimg_output, cy3img_mask = straightenCropAndMask(np.array(cy3img),heimg_align_color,cy3_spots,margin=100)
		if display:
			plt.figure()
			plt.imshow(cy3img_mask,"gray")
			plt.imshow(heimg_output,alpha=0.75)
			plt.show()
		if saveTo is not None:
			Image.fromarray(cy3img_output).save(saveTo+"_Cy3_aligned.jpg")
			Image.fromarray(heimg_output).save(saveTo+"_HE_aligned.jpg")
			Image.fromarray(cy3img_mask).save(saveTo+"_Cy3_mask.jpg")

	return d, M, cy3_spots, he_spots

''' Perform batch alignment on pairs of images originating from the same slide, which should be transformed in the same way.
	- Attempts alignment of each matching pair of images individually.
	- Applies affine transform from most successful alignment (as defined by highest overlap of detected spots) to all images.
'''
def alignSlide(cy3files, hefiles, arraySize, display=False, postProcessing=False):
	assert len(cy3files)==len(hefiles), "Lists of corresponding Cy3 and HE images are of different length!"

	bestOverlap, bestM = 0, None
	bestSlide, bestSpots = None, None

	for i,(cy3,he) in enumerate(zip(cy3files, hefiles)):
		try:
			d, M, cy3_spots, he_spots = alignImages(cy3, he, arraySize, display, postProcessing)

			if d >= bestOverlap:
				bestOverlap = d
				bestM = M
				bestSlide = he
				bestSpots = cy3_spots
		except RuntimeError as err:
			print(err)

	cy3images = [imread(cy3) for cy3 in cy3files]
	heimages = [imread(he) for he in hefiles]

	for cy3img, heimg in zip(cy3images,heimages):
		heimg_warp = cv2.warpAffine(heimg, bestM, (cy3img.shape[1],cy3img.shape[0]))
		blended_img = cv2.addWeighted(heimg_warp,0.75,cy3img,0.25,0)
		plt.figure()
		plt.imshow(blended_img)
		plt.show()
		plt.close()


#################################################################################
##########					TESTING ROUTINES					#################
#################################################################################

# Base test of spot alignment - performs alignment on the six wells of the L756 slide.
def TestL756():
	spotDir = "data/Cy3/"
	histoDir = "data/HE/"

	for i in range(1,7):
		print("===> L756_s%d"%i)
		cy3 = spotDir + "20171107_L756_Cy3_stitch_stitch_s%d.jpg"%i
		he = histoDir + "20171106_L7_56_HE_stitch_stitch_s%dc1+2+3.jpg"%i
		alignImages(cy3, he, [35,33], display=True, postProcessing=True, presentGUI=True)

# Tests batch alignment of images on the same slide, where affine xform from one can be applied to others without detectable keypoints.
def TestSlideL7816():
	spotDir = "data/Cy3/"
	histoDir = "data/HE/"

	cy3images = [spotDir + '20171107_L816_Cy3_stitch_stitch_s%d.jpg'%i for i in range(5,7)]
	heimages = [histoDir + '20171031_L8_16_HE_stitch_stitch_s%dc1+2+3.jpg'%i for i in range(5,7)]
	
	alignSlide(cy3images,heimages,[35,33],display=False)

# Attempts alignment of all matching images in CGND directory.
def TestAllMatched():
	spotDir = os.path.expanduser('~/Desktop/CGND/Cy3_stitched/')
	histoDir = os.path.expanduser('~/Desktop/CGND/HE_stitched/')
	count = 0

	spotFiles, histoFiles = findMatchedFiles(spotDir,histoDir)
	for cy3,he in zip(spotFiles, histoFiles):
		print("=============")
		print(cy3)
		print(he)
		try:
			alignImages(cy3,he,[35,33],display=False)
			count += 1
		except RuntimeError as e:
			print(e)
	print("ALIGNED",count,"OUT OF",len(spotFiles),"IMAGES")

# Attempts to align the 20x images provided by Silas
def TestFUS():
	spotDir = os.path.expanduser("~/Desktop/CGND/FUS_Cy3/")
	histoDir = os.path.expanduser("~/Desktop/CGND/FUS_HE/")

	cy3file = spotDir + "L8CN27_Cy3_max_max_s2.jpg"
	hefile = histoDir + "L827_HE_max_max_s2c1+2+3.jpg"
	alignImages(cy3file, hefile, [35,33], display=False, postProcessing=True, presentGUI=True, saveTo="L8CN27_s2")


############### ###############

import os
import argparse

from fileprocessing import findMatchedFiles

if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("cy3file", type=str, help="Path to Cy3 file")
	parser.add_argument("hefile", type=str, help="Path to HE file")
	parser.add_argument("h_st", type=int, help="Height of ST array (spots)")
	parser.add_argument("w_st", type=int, help="Width of ST array (spots)")
	parser.add_argument("-o", "--outfile", type=str, default=None, help="Base name for output files")
	args = parser.parse_args()

	alignImages(args.cy3file, args.hefile, [args.w_st, args.h_st], 
		display=True, postProcessing=True, presentGUI=True,
		saveTo=args.outfile)

	# Test alignment of 6 representative images
	#TestL756()

	# Test batch alignment of images on a slide
	#TestSlideL7816()

	# Test alignment of all matched images received from Silas
	#TestAllMatched()

	#TestFUS()

	'''countFile = "data/L7CN56_D1_stdata_aligned_counts_IDs.txt")
	spots, counts, genes = readCountData(countFile)

	xdim, ydim = cy3img.shape
	pixel_dim = 194.0/(6200.0/xdim)

	xcoords = pixel_dim * (spots[:,0]-1)
	ycoords = ydim - pixel_dim * (spots[:,1]-1)'''







