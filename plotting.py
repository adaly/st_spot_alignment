from matplotlib import pyplot as plt
import numpy as np
import cv2

#################################################################################
##########			ST ALIGNMENT PLOTTING ROUTINES				#################
#################################################################################

# Plots a list of images, with optional titles and color modes, in rows for comparison.
def plotCompare(imgs, titles=None, colorModes=None, saveTo=None):
	nplots = len(imgs)
	maxcols = 4
	ncols = min(maxcols,nplots)
	nrows = int(np.ceil(nplots/ncols))

	plt.figure()
	for i in range(nplots):
		ax = plt.subplot(nrows,ncols,i+1)

		if isinstance(colorModes,list) and len(colorModes) == nplots:
			plt.imshow(imgs[i], colorModes[i])
		elif isinstance(colorModes,str):
			plt.imshow(imgs[i], colorModes)
		else:
			plt.imshow(imgs[i], 'gray')

		if isinstance(titles,list) and len(titles) == nplots:
			ax.set_title(titles[i])
	
	if saveTo != None:
		plt.savefig(saveTo,format='jpg',dpi=300)
	plt.show()

# Substitute for cv2.drawKeypoints() which doesn't seem to work on OSX (recognized problem in OpenCV 4.0.0.21)
def drawKeypoints(img,keypoints,saveTo=None,xgrid=[],ygrid=[]):	
	plt.figure()
	plt.suptitle("Proposed Alignment")

	cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
	plt.imshow(cimg)
	
	if len(keypoints) != 0:
		pts = np.array([k.pt for k in keypoints])
		pts_x, pts_y = pts[:,0], pts[:,1]

		plt.scatter(pts_x,pts_y,alpha=0.5,marker='x',c='r')

	if len(xgrid) > 0:
		for xl in xgrid:
			plt.axvline(xl)
		plt.xticks(xgrid,[str(s) for s in range(len(xgrid))],fontsize=7,rotation=90)
	if len(ygrid) > 0:
		for yl in ygrid:
			plt.axhline(yl)
		plt.yticks(ygrid,[str(s) for s in range(len(ygrid))],fontsize=7)

	if saveTo is not None:
		plt.savefig(saveTo,format='jpg',dpi=300)
	plt.show()

# Presents an interactive plot to the user showing detected keypoints:
# - Spurious can be removed with right-click
# - Undetected keypoints can be manually added with left-click. 
# 
# As of now, size of added keypoint fixed to mean size of other keypoints, though connected component analysis
#  or blob detection might be a better (if more complicated) strategy.
# 
# Returns a list of curated keypoints.
def curateKeypoints(img, keypoints, spot_size=None):	
	fig = plt.figure()
	plt.suptitle("Left-click: add, Right-click: remove")

	cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
	plt.imshow(cimg)
	
	if len(keypoints) != 0:
		pts = np.array([k.pt for k in keypoints])
		pts_x, pts_y = pts[:,0], pts[:,1]

		xydata = plt.scatter(pts_x,pts_y,alpha=0.5,marker='.',c='r')
	else:
		# Placeholder point
		xydata = plt.scatter([img.shape[0]/2],[img.shape[1]/2],alpha=0.5,marker='.',c='r')

	remaining_keypoints = keypoints

	# Add on-click event handler so that we can add/remove keypoints.
	def onclick(event):
		nonlocal remaining_keypoints	# In Python 3, allows rebinding of local variables from enclosing function.
		nonlocal spot_size
		
		# On left-click, attempt to locate keypoint
		if event.button == 1:
			if spot_size is None:
				# Add a new keypoint with size equal to mean size of current keypoints
				spot_size = np.mean([k.size for k in remaining_keypoints])
			remaining_keypoints.append(cv2.KeyPoint(event.xdata, event.ydata, spot_size))
			print("Added keypoint at",remaining_keypoints[-1].pt,"of size",spot_size)
			# Update plot
			new_pts = [k.pt for k in remaining_keypoints]
			xydata.set_offsets(new_pts)
			plt.draw()

		# On right-click, delete keypoint
		if event.button == 3:
			# Find nearest existing point
			min_dist = np.inf
			for i,k in enumerate(remaining_keypoints):
				dst = (k.pt[0]-event.xdata)**2 + (k.pt[1]-event.ydata)**2
				if dst < min_dist:
					min_dist = dst
					nearest_pt_ind = i
			nearest_pt = remaining_keypoints[nearest_pt_ind].pt
			
			# Check if click was sufficiently close to a point
			if min_dist < 5000:
				# Remove keypoint corresponding to selected point
				remaining_keypoints.pop(nearest_pt_ind)
				print("Removed", nearest_pt, ",", len(remaining_keypoints), "keypoints remain")
				# Update plot
				new_pts = [k.pt for k in remaining_keypoints]
				xydata.set_offsets(new_pts)
				plt.draw()

	cid = fig.canvas.mpl_connect('button_press_event', onclick)
	plt.show()

	return remaining_keypoints
