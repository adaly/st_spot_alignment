# README #

Spot alignment tool for vanilla ST experiments.

### Prerequisites ###

* Python 3 with all the whistles (numpy, matplotlib)
* OpenCV
* [SKImage](https://scikit-image.org/docs/dev/api/skimage.html)
* [Pillow](https://pillow.readthedocs.io/en/stable/index.html)
* [ST Tissue Recognition](https://github.com/SpatialTranscriptomicsResearch/st_tissue_recognition)

### Usage ###

```
python spotalign.py PATH_TO_CY3_IMG_FILE PATH_TO_HE_IMG_FILE ST_ARRAY_HEIGHT ST_ARRAY_WIDTH [PATH_TO_OUTPUT]
```