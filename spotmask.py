import imageio
import matplotlib.pyplot as plt
import numpy as np

# Images can be quite large:
from PIL import Image
Image.MAX_IMAGE_PIXELS = 1000000000

from scipy import ndimage

# Removes noise from binary image through use of erosion and dilation operations.
def denoiseSpots(spotFile,erosionDiam=10,showPlot=False):
	# Invert image so that spots are white (max valued)
	s1 = np.invert(spotFile)
	# Convert to binary matrix
	s2 = np.maximum.reduce(s1>s1.mean(),axis=2)

	# 1. Erosion -- replaces value of a pixel by minimum value covered by Structure 
	#    (defaults to 4 adjacent pixels). Can be iterated.
	# 2. Dilation -- reverse of erosion; uses maximum value. Mask can be applied to 
	#    make sure no pixels that were off in the original turn on.
	#    binary_propagation iterates dilation until there is no change.
	X,Y = np.ogrid[0:erosionDiam,0:erosionDiam]
	structure = (X - erosionDiam/2)**2 + (Y - erosionDiam/2)**2 < (erosionDiam/2)**2. # Erosion/Dilation over circle of radius diam.
	eroded_spots = ndimage.binary_erosion(s2,structure=structure,iterations=4) # Removes most blotches
	dilated_spots = ndimage.binary_propagation(eroded_spots,mask=s2,structure=structure) # Expands spots to original size
	final_erosion = ndimage.binary_erosion(dilated_spots,iterations=2) # Removes some noisy pixels around spot edges

	# Plotting function that shows results of erosion/dilation.
	if showPlot:
		plt.figure(figsize=(9.5,4))
		ax = plt.subplot(141)
		plt.imshow(s2,cmap=plt.cm.gray, interpolation='nearest')
		ax.set_title('Inverted Image')
		plt.axis('off')
		ax = plt.subplot(142)
		plt.imshow(eroded_spots,cmap=plt.cm.gray, interpolation='nearest')
		ax.set_title('Erosion 1')
		plt.axis('off')
		ax = plt.subplot(143)
		plt.imshow(dilated_spots,cmap=plt.cm.gray, interpolation='nearest')
		ax.set_title('Dilation 1')
		plt.axis('off')
		ax = plt.subplot(144)
		plt.imshow(final_erosion,cmap=plt.cm.gray, interpolation='nearest')
		ax.set_title('Erosion 2')
		plt.axis('off')
		plt.subplots_adjust(wspace=0, hspace=0.02, top=0.99, bottom=0.01, left=0.01, right=0.99)
		plt.show()

	return np.invert(final_erosion), final_erosion

# Labels all connected components of a spot image, and removes those that are too large/small to be single spots.
def labelSpots(spotImage,minSpotSize,maxSpotSize):
	# Finds all connected components and assigns them a label.
	#  label_im -- ndarray of ints indicating the label for each pixel.
	#  nb_labels -- number of unique labels.
	label_im, nb_labels = ndimage.label(spotImage)

	# Calculate sizes of all connected components
	sizes = ndimage.sum(spotImage, label_im, range(nb_labels+1))

	# Perform initial de-noising, then calculate min spot size from median connected component size.
	if minSpotSize == None:
		denoisedSizes = []
		for x in sizes:
			if x > 100:
				denoisedSizes.append(x)
		medianSpotSize = np.median(denoisedSizes)
		print(medianSpotSize)

		minSpotSize = 0.5*medianSpotSize

	if maxSpotSize == None:
		maxSpotSize = 3*minSpotSize

	# Remove connected components that are too small (noise on the image, incomplete spots)
	mask_size = sizes < minSpotSize
	remove_pixel = mask_size[label_im]
	label_im[remove_pixel] = 0

	# Remove connected components that are too big (merged spots)
	# TODO: Pre-processing of the image (erosion) might also help with merged spots.
	mask_size = sizes > maxSpotSize
	remove_pixel = mask_size[label_im]
	label_im[remove_pixel] = 0

	# Re-label graph now that small patches have been removed.
	labels = np.unique(label_im)
	label_im = np.searchsorted(labels,label_im)

	return label_im

# Accepts aligned histology and spot images, and masks each detected spot against the histology image, creating a separate file.
def maskImageSpots(spotfile,histofile,outputfile="spot",outputext=".jpg",minSpotSize=None,maxSpotSize=None,paddedShape=None):

	print((spotfile.split("/")[-1]).split("_spots")[0])

	# For some reason, if I read in the HE image first, it is scaled down/cropped to 768x1024, but reading spots first is fine.
	# Perhaps a result of the DecompressionBombWarning that is triggered, but only affects HE image...
	spots = imageio.imread(spotfile)
	he = imageio.imread(histofile)

	assert he.shape == spots.shape, "Images not correctly scaled!"

	denoisedSpots, invertedSpots = denoiseSpots(spots,showPlot=False)

	# HE image masked with spot locations -- edges currently a bit chunky.
	he[denoisedSpots] = 0

	# Label all unique connected components (spots), so that we may construct individual masks for each.
	label_im = labelSpots(invertedSpots, minSpotSize, maxSpotSize)

	# Max value of new label_im denotes number of connected components (spots)
	nspots = label_im.max()
	if nspots == 0:
		print("No spots detected...")
		return
	print("Number of spots: ", nspots)

	# Find bounding box containing each connected component, and use to extract from HE image!
	# Note that class 0 corresponds to background, and should be ignored.
	max_x,max_y = 0,0
	bbox_locs = np.zeros((nspots,4))
	for i in range(1,nspots+1):
		slice_x, slice_y = ndimage.find_objects(label_im==i)[0]
		spot_coords = np.array([slice_x.start, slice_x.stop, slice_y.start, slice_y.stop])
		bbox_locs[i-1] = spot_coords
		if paddedShape == None:
			max_x = max(max_x, spot_coords[1]-spot_coords[0])
			max_y = max(max_y, spot_coords[3]-spot_coords[2])
		else:
			max_x,max_y = paddedShape

	# Pad sliced images with zeros before saving, so that all output images are of same dimension.
	# Additionally, ensure all images are square to facilitate rescaling images from different slides to a common size
	#   without stretching/compressing features (handled by keras preprocessing).
	for bbox in bbox_locs:
		slice_x = slice(int(bbox[0]),int(bbox[1]))
		slice_y = slice(int(bbox[2]),int(bbox[3]))
		#print("x: ", slice_x.start, "-", slice_x.stop, "y: ", slice_y.start, "-", slice_y.stop)
		roi = he[slice_x, slice_y]

		square_len = max(max_x,max_y)
		xdif = float(square_len - (bbox[1]-bbox[0]))/2
		ydif = float(square_len - (bbox[3]-bbox[2]))/2
		xpad = ( int(np.floor(xdif)), int(np.ceil(xdif)) )
		ypad = ( int(np.floor(ydif)), int(np.ceil(ydif)) )
		roi = np.pad(roi,(xpad,ypad,(0,0)),'constant')

		# Naming convention: BaseName_xcenter_ycenter
		spot_coords = [slice_x.start+slice_x.stop//2, slice_y.start+slice_y.stop//2]
		fname = outputfile+"_"+"_".join([str(x) for x in spot_coords])+outputext
		imageio.imwrite(fname,roi)
	print("Output image dimensions:",roi.shape)

import os, glob

# Performs per-spot masking on images with matching base names:
#   BASENAME_HE.jpg will be masked with BASENAME_spots.jpg
#   Individual spot images will be saved in destdir as BASENAME_spot_[coords].jpg
def maskAllSpots(spotdir,histodir,destdir,ext='.jpg'):
	
	for file in os.listdir(spotdir):

		if file.endswith(ext):

			# Some of the L8 slices represent human samples -- stick to mouse for now
			#   Mouse slides have ~80 spots per slice, 1-4 slices per slide.
			#   Human slides have ~900 spots per slice, 1 slice per slide.
			if file[0:2] == "L8":
				continue

			basename = file.split('_spots')[0]
			spotfile = spotdir + file
			histofile = histodir + basename + "_HE" + ext
			destfilebase = destdir + basename + "_spot"

			# If we have already processed this image, skip it.
			if len(glob.glob(destfilebase+"*")) != 0:
				print("Already processed ", basename)
				continue

			if os.path.isfile(histofile):
				try:
					# NOTE: Providing numerical values to min/max spot size causes some slides (L7bCN03_E1, e.g.)
					# to be marked as having no spots. Defaulting values to None chooses min/max spot size based
					# on median size after de-noising, but without a good noise threshold this could also cause errors.
					maskImageSpots(spotfile,histofile,destfilebase,minSpotSize=8e3,maxSpotSize=2.4e4)
				# For example, dimensions of HE and Spot file for CN93_C2 are not consistent
				except AssertionError as error:
					print(basename, error)


if __name__ == "__main__":
	#spotfile = os.path.expanduser('~/Dropbox (Simons Foundation)/Scaled/Spots/CN69_D2_spots.jpg')
	#hefile = os.path.expanduser('~/Dropbox (Simons Foundation)/Scaled/HE/CN69_D2_HE.jpg')
	#base = "CN69_D2_spot"

	#maskImageSpots(spotfile,hefile,base)

	spotdir = os.path.expanduser('~/Dropbox (Simons Foundation)/Scaled/Spots/')
	histodir = os.path.expanduser('~/Dropbox (Simons Foundation)/Scaled/HE/')
	maskAllSpots(spotdir,histodir,'imagedata/train_spots/allslides/')


