import re, glob
import csv

''' Find all files that share a common base name, such as L756_s1
'''
def findMatchedFiles(spotDir,histoDir):
	base = "([0-9]+)_.*(L[0-9]+b?)_?([0-9]*).+(s[0-6])"

	spotFiles = glob.glob(spotDir+"*.jpg")
	histoFiles = glob.glob(histoDir+"*.jpg")

	spotFilesMatched, histoFilesMatched = [], []

	for sf in spotFiles:
		m = re.search(base,sf)

		if m == None:
			print("Weird format:",sf)
			continue

		mg = ''.join(m.groups()[1:])

		for hf in histoFiles:
			n = re.search(base,hf)

			if n == None:
				print("Weird format:",hf)
				continue

			ng = ''.join(n.groups()[1:])

			if mg == ng:
				spotFilesMatched.append(sf)
				histoFilesMatched.append(hf)

	return spotFilesMatched, histoFilesMatched

# Read UMI count data for spots in the image (specified in a separate file).
# The fact that only spots under the tissue are will have significant UMI reads can be used 
#   to either guide or evaluate the alignment process.
def readCountData(fileName, delimiter='\t'):
	csvFile = open(fileName)
	reader = csv.reader(csvFile, delimiter=delimiter)

	lineCount = 0
	spotCoords, geneNames, countMatrix = [],[],[]
	for row in reader:
		# First line contains spot coordinates in form X_Y
		if lineCount == 0:
			for coordStr in row:
				pt = [float(x) for x in coordStr.split('_')]
				spotCoords.append(pt)
		else:
			# First entry in each subsequent row is a gene name, followed by a list of ints dictating
			#   UMI counts for that gene in the spot corresponding to that column.
			geneNames.append(row[0])
			umiCounts = [int(x) for x in row[1:]]
			countMatrix.append(umiCounts)
		lineCount += 1
		
	spotCoords = np.array(spotCoords)
	countMatrix = np.array(countMatrix)

	return spotCoords, countMatrix, geneNames