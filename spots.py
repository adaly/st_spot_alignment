# -*- coding: utf-8 -*-

import itertools as it

import numpy as np
from sklearn.cluster import KMeans

from circle_detector import CircleDetector, DetectionType


CIRCLE_DETECTOR = CircleDetector()

from matplotlib import pyplot as plt 

# Binary search -- used for finding closest grid line (xs) to a given point x in 1D space.
def _bin_search(xs, x):
    def _do(l, h):
        if h - l == 1:
            return l, h
        m = (h + l) // 2
        if xs[m] >= x:
            return _do(l, m)
        return _do(m, h)
    l, h = _do(0, len(xs) - 1)
    return h if xs[h] - x < x - xs[l] else l

class Spots:
    """Holds the spot data. These spots are stored with positions relative
    to the originally-uploaded Cy3 image.
    """
    # pylint: disable=invalid-name

    def __init__(self, array_size, scaling_factor):
        self.spots = []
        self.tissue_spots = []
        self.array_size = array_size
        self.scaling_factor = scaling_factor
        self.tl = [0, 0]
        self.br = [0, 0]
        # Map from grid coordinates of spots to pixel x and y coordinates.
        self.Gx = []
        self.Gy = []
        self.Gs = []
        self.xmeans = []
        self.ymeans = []

    def wrap_spots(self):
        """Wraps detected spot positions and calibration coordinates in dict"""
        return {
            'positions': self.spots.tolist(),
            'tl': self.tl.tolist(),
            'br': self.br.tolist(),
        }

    def is_spot_at_gridpoint(self,x,y):
        """Returns True if there is a spot at the gridpoint indicated by indices x,y, False otherwise"""
        if np.array(self.Gx).size == 0 and np.array(self.Gy).size == 0:
            raise RuntimeError("Spot grid not yet instantiated!")
        if x >= self.array_size[0] or y >= self.array_size[1]:
            raise RuntimeError("Attempting to access spot at location beyond grid boundaries!")

        return (self.Gx[x][y] > 0 and self.Gy[x][y] > 0)

    def get_spot_at_gridpoint(self,x,y):
        """Returns pixel coordinates of spot cetered at gridpoint [x,y]"""
        return [self.Gx[x][y], self.Gy[x][y]]
    def get_spot_size_at_gridpoint(self,x,y):
        """Returns radius of spot cetered at gridpoint [x,y]"""
        return self.Gs[x][y]/2

    def get_gridpoint_at_spot(self,px,py):
        """Returns grid coordinates of spot found at pixel coordinates px,py"""
        i = _bin_search(self.xmeans,px)
        j = _bin_search(self.ymeans,py)
        return [i,j]

    def get_rotation(self):
        """Calculates angle by which image must be rotated so that spots are aligned horizontally/vertically on screen"""
        """Returns center, angle"""
        longest_row = []
        
        # Find the row containing the most points.
        for row in range(self.array_size[1]):
            pts = []
            for col in range(self.array_size[0]):
                if self.is_spot_at_gridpoint(col,row):
                    pts.append(self.get_spot_at_gridpoint(col,row))
            if len(pts) > len(longest_row):
                longest_row = pts
        
        # Find the slope of best fit between these points, and use angle to calculate rotation.
        longest_row = np.array(longest_row)
        X, Y = longest_row[:,0], longest_row[:,1]
        m = ((X*Y).mean() - X.mean()*Y.mean()) / ((X**2).mean() - (X.mean())**2)

        return tuple(longest_row[0]), np.degrees(np.arctan(m))

    def distance_from_grid(self):
        """Calculates total distance of keypoint centers from their assigned grid position"""
        dist = 0.
        for i in range(self.array_size[0]):
            for j in range(self.array_size[1]):
                if self.is_spot_at_gridpoint(i,j):
                    xcor, ycor = self.get_spot_at_gridpoint(i,j)
                    dist += np.sqrt((self.xmeans[i]-xcor)**2 + (self.ymeans[j]-ycor)**2)
        return dist

    def create_spots_from_keypoints(self, keypoints, thresholded_image):
        """Takes keypoints generated from opencv spot detection and
        tries to match them to their correct array positions.
        It also tries to fill in "missing spots" by finding which array
        positions do not have a corresponding keypoint, then analyses the
        pixels around that position to determine if a spot is likely
        to be there or not.
        """
        if len(keypoints) < max(*self.array_size):
            raise RuntimeError('Too few keypoints')

        spots = np.array(list(zip(*[x.pt + (x.size,) for x in keypoints])))

        xmeans, ymeans = [
            np.sort([np.mean(zs[c == k]) for k in range(n)])
            for zs, n, c in zip(
                spots,
                self.array_size,
                [
                    KMeans(n_clusters=k).fit_predict(np.transpose([zs]))
                    for k, zs in zip(self.array_size, spots)
                ]
            )
        ]

        bins = np.zeros(self.array_size)
        # TODO: 0 denotes lack of spot at grid coordinate -- could be confusing if there were a spot centered at 0.
        self.Gx = np.zeros(self.array_size)
        self.Gy = np.zeros(self.array_size)
        self.Gs = np.zeros(self.array_size)

        for x, y, s in zip(*spots):
            xind = _bin_search(xmeans, x)
            yind = _bin_search(ymeans, y)
            bins[xind, yind] += 1
            # Save arrays detailing the pixel x and y values for points at a given grid index.
            # NOTE: Would need to be updated to include missing spots added later.
            self.Gx[xind,yind] = x
            self.Gy[xind,yind] = y
            self.Gs[xind,yind] = s

        missing_spots = []
        for x, y in it.product(*map(range, self.array_size)):
            if bins[x, y] == 0:
                missing_spots.append((x, y))

        image_pixels = thresholded_image.load()

        # Spots "filled in" by examining missing grid points will have size=0.
        for x, y in missing_spots:
            for res in (
                    CIRCLE_DETECTOR.detect_spot(
                        t,
                        image_pixels,
                        (xmeans[x], ymeans[y]),
                    ) for t in (
                        DetectionType.EDGES,
                        DetectionType.WHITENESS,
                    )
            ):
                if res is not None:
                    spots = np.concatenate([
                        spots,
                        np.transpose([res + (0,)]),
                    ], axis=1)
                    break

        # Array of N spots in form [xcoord, ycoord, diameter]
        self.spots = np.transpose(spots) * self.scaling_factor
        self.tl, self.br = np.transpose([
            xmeans[[0, -1]],
            ymeans[[0, -1]],
        ]) * self.scaling_factor
        self.xmeans = xmeans
        self.ymeans = ymeans

    def create_spots_from_keypoints_sparse(self, keypoints, spotDist):
        """Unlike the above method, which was designed for Cy3 image files and depends on having enough spots
        to cover the grid (defined by kmeans clustering), this method is designed to fit a minimum grid
        around the potentially sparse spots detected on an HE image file.
        
        NOTE: As this method determines array size on the fly, the array_size argument provided when instantiating
        the class is irrelevant. Perhaps we should modify the __init__ function to take a keypoints argument, make
        array_size optional, and automatically call the correct create_spots method depending on if it is provided.
        """
        spots = np.array(list(zip(*[x.pt + (x.size,) for x in keypoints])))
        ycoords = np.sort([k.pt[1] for k in keypoints])
        xcoords = np.sort([k.pt[0] for k in keypoints])

        xspace, yspace = spotDist
        ncols = np.ceil((xcoords[-1]-xcoords[0])/xspace).astype(int)
        nrows = np.ceil((ycoords[-1]-ycoords[0])/yspace).astype(int)

        ncols = min(ncols,self.array_size[0])
        nrows = min(nrows,self.array_size[1])
        self.array_size = [ncols,nrows]

        # Takes sorted list of points and returns bins of points within range delta.
        def _bin_points(points,delta):
            bins = []
            newbin = [points[0]]
            for i in range(1,len(points)):
                if points[i]-points[i-1] > delta:
                    bins.append(newbin)
                    newbin = [points[i]]
                else:
                    newbin.append(points[i])
            bins.append(newbin)
            return bins

        xbins = _bin_points(xcoords,.5*xspace)
        ybins = _bin_points(ycoords,.5*yspace)

        tl = [np.mean(xbins[0]),np.mean(ybins[0])]
        br = [np.mean(xbins[-1]),np.mean(ybins[-1])]

        xmeans = [tl[0] + (br[0]-tl[0])/(ncols-1) * i for i in range(ncols)]
        ymeans = [tl[1] + (br[1]-tl[1])/(nrows-1) * i for i in range(nrows)]

        # TODO: 0 denotes lack of spot at grid coordinate -- could be confusing if there were a spot centered at 0.
        self.Gx = np.zeros(self.array_size)
        self.Gy = np.zeros(self.array_size)
        self.Gs = np.zeros(self.array_size)

        # Compute array mapping integer indices in the grid to pixel coordinates for spot center.
        for x, y, s in zip(*spots):
            xind = _bin_search(xmeans, x)
            yind = _bin_search(ymeans, y)
            # Save arrays detailing the pixel x and y values for points at a given grid index.
            self.Gx[xind,yind] = x
            self.Gy[xind,yind] = y
            self.Gs[xind,yind] = s

        self.spots = np.transpose(spots) * self.scaling_factor
        self.tl = np.array(tl) * self.scaling_factor
        self.br = np.array(br) * self.scaling_factor
        self.xmeans = xmeans
        self.ymeans = ymeans


