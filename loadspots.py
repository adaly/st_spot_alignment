import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import os, glob

train_dir = 'imagedata/train_spots/'	# Must contain at least one sub-directory of images.
test_dir = 'imagedata/test_spots/'		# Currently, test_spots has 1290 images (CN95-96), and train_spots has 13301 (the rest).
nimages_train = 15000	# Number of training examples to use -- can be higher than actual number of images due to data augmentation.
pct_val = 0.2			# Proportion of training set to hold out for validation.
batch_size = 128			
target_size = (150,150)	# Size to which all images will be rescaled.

import datetime
now = datetime.datetime.now()
suffix = "%d_%d" % (now.month,now.day)

# Defines data augmentation steps that will be taken when generating batches of training images.
train_datagen = ImageDataGenerator(
	rescale=1./255,
	horizontal_flip=True,
	vertical_flip=True,
	rotation_range=90,
	validation_split=pct_val)

# The only processing for test image data will be rescaling.
test_datagen = ImageDataGenerator(rescale=1./255)

# Pulls data from all sub-directories of train_dir, treating each one as a separate class
# (the class assignments don't matter for autoencoder)
train_generator = train_datagen.flow_from_directory(
	train_dir,
	target_size=target_size,
	batch_size=batch_size,
	color_mode='rgb',
	class_mode='input',		# Denotes that this is an autoencoder.
	subset='training',
	shuffle=True)

val_generator = train_datagen.flow_from_directory(
	train_dir,
	target_size=target_size,
	batch_size=batch_size,
	color_mode='rgb',
	class_mode='input',
	subset='validation',
	shuffle=True)

test_generator = test_datagen.flow_from_directory(
	test_dir,
	target_size=target_size,
	batch_size=batch_size,
	color_mode='rgb',
	class_mode=None,
	shuffle=False
	)

# Construct the input layer and CNN
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D, UpSampling2D, Dense 
from tensorflow.keras.models import Model 

# All input images will be 3-channel, with height and width defined by target_size
input_img = Input(shape=(target_size)+(3,))

# NOTE: Although it is called Conv2D, it has a third dimension that matches the number of channels.
# The "2D" part denotes that the filter moves in a 2-dimensional fashion across the image, looking at all channels simultaneously.
x = Conv2D(16, (11,11), padding='same', activation='relu')(input_img)
x = MaxPooling2D((3,3), padding='same')(x)
x = Conv2D(32, (5,5), padding='same', activation='relu')(x)
encoded = MaxPooling2D((2,2), padding='same')(x)

x = UpSampling2D((2,2))(encoded)
x = Conv2D(16, (5,5), padding='same', activation='relu')(x)
x = UpSampling2D((3,3))(x)
decoded = Conv2D(3, (11,11), padding='same', activation='sigmoid')(x)

encoder = Model(input_img, encoded)
autoencoder = Model(input_img, decoded)
autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')

history = autoencoder.fit_generator(
	train_generator,
	steps_per_epoch=nimages_train//batch_size,
	epochs=20,
	validation_data=val_generator,
	validation_steps=nimages_train * pct_val //batch_size)
autoencoder.save_weights('spot_autoencoder_%s.h5' % suffix)
#autoencoder.load_weights('spot_autoencoder_3_11.h5')


# See how the model did
import matplotlib.pyplot as plt 

test_in = test_generator.next()
test_out = autoencoder.predict(test_in)


n=10
plt.figure(figsize=(20, 4))
for i in range(n):
	img_in = (test_in[i] * 255).astype('int')
	
	ax = plt.subplot(2, n, i+1)
	plt.imshow(img_in)
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)

	img_out = (test_out[i] * 255).astype('int')

	ax = plt.subplot(2, n, i+n+1)
	plt.imshow(img_out)
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)

plt.savefig('results/spot_aec_test_%s.jpg' % suffix,format='jpg',dpi=300)
plt.show()





